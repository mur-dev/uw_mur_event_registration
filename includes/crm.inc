 <?php

/**
 * @file
 * Functions involving calls to the crm.
 */

/**
 * CRM call to create a lead
 * Assume the user is already successfully authenticated
 */
function uw_event_registration_get_session_times($session_name) {
  uw_mur_event_registration_crm_authorization($session_id, $url);
  //get event times from tour schedule module

  $sql_filter = " name = '" . $session_name . "' AND current_attendees_c < max_attendees";

  $session_schedules = array();
  $get_entries_list_parameters = array(
    'session' => $session_id,
    'module_name' => 'evt_event_session',
    'query' => $sql_filter,
    'order_by' => "form_sort_order_c asc",
    'offset' => 0,
    'select_fields' => array(
      'id',
      'name',
      'session_datetime',
    ),
    'link_name_to_fields_array' => array(),
    'max_results' => 100,
    'deleted' => 0,
    'favorites' => FALSE,
  );

  $schedule_result = uw_mur_event_registration_crm_call("get_entry_list", $get_entries_list_parameters, $url);

  foreach ($schedule_result["entry_list"] as $item) {
    array_push($session_schedules, $item["name_value_list"]);
  }


  return $session_schedules;
}
